const admin = require("firebase-admin");

const serviceAccount = require("../membermanagement-77d73-firebase-adminsdk-49vhj-e413117147");

admin.initializeApp({
  credential: admin.credential.cert(serviceAccount),
  databaseURL: "https://membermanagement-77d73-default-rtdb.firebaseio.com"
});

const db = admin.database();

module.exports = db;