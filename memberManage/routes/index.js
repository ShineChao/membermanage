const express = require('express');
const router = express.Router();
const firebaseDb = require('../connections/firebase_admin_connect');
const firebase = require('../connections/firebase_connect');

router.get('/', function (req, res) {
    firebaseDb.ref('list').once('value',function(snapshot){
        const auth = req.session.uid;
        res.render('index', {
            title: '求職留言板',
            auth: auth,
            error: req.flash('errors'),
            list:snapshot.val()
        });
    })

    // 測試有連到firebase.auth
    // console.log(firebase.auth());

    // 測試連到fireBase的Db，如連到 console.log"hello"
    // firebaseDb.ref().once('value',function(snapshot){
    //     console.log(snapshot.val());
    // })
});
/* GET home page. */
module.exports = router;