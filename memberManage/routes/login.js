const express = require('express');
const router = express.Router();
const firebase = require('../connections/firebase_connect');
const firebaseDb = require('../connections/firebase_admin_connect')
const fireAuth = firebase.auth();

router.get('/', function (req, res) {
    res.render('login', { title: '登入' });
})
router.post('/', function (req, res) {
    fireAuth.signInWithEmailAndPassword(req.body.email, req.body.passwd)
    .then(function(user){
        req.session.uid = user.uid;
        res.redirect('/');

    })
    .catch(function(error){
        res.redirect('/')
        console.log(error);
        console.log('登入失敗');
    })
})
module.exports = router;