const express = require('express');
const router = express.Router();
const firebaseDb = require('../connections/firebase_admin_connect');
router.post('/', function(req, res){
    

    firebaseDb.ref('user/'+req.session.uid).once('value',function(snapshot){
        const nickname = snapshot.val().nickname;
        const ref = firebaseDb.ref('list').push();
        const listContent = {
            nickname: nickname,
            content : req.body.content
        }
        ref.set(listContent)
        .then(function(){
            res.redirect('/')
        })
    })
})
module.exports = router