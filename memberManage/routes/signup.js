const express = require('express');
const router = express.Router();
const firebase = require('../connections/firebase_connect');
const firebaseDb = require('../connections/firebase_admin_connect')
const fireAuth = firebase.auth();
router.get('/', function (req, res) {
    res.render('signup', { title: '註冊', error: req.flash('error')});
})

router.post('/', function (req, res) {

    const email = req.body.email;
    const password = req.body.passwd;
    const nickname = req.body.nickname;

    // 儲存會員註冊資料並將會員 email、nickname、uid存於Db
    fireAuth.createUserWithEmailAndPassword(email, password)
    .then(function(user){
        console.log(user);
        const saveUser = {
            'email':email,
            'nickname':nickname,
            'uid':user.uid
        }

        firebaseDb.ref('/user/'+user.uid).set(saveUser)
        res.redirect('/signup/success')
    })
    .catch(function(error){
        console.log(error)
        const errorMessage = error.message;
        req.flash('error',errorMessage)
        res.redirect('/signup')
    })
})
router.get('/success',function(req,res){
    res.render('success',{
        title:'註冊成功'
    });
})
module.exports = router;