const express = require('express');
const router = express.Router();
const firebaseDb = require('../connections/firebase_admin_connect')

router.get('/', function (req, res) {
    console.log('req.session.uid',req.session.uid)
    firebaseDb.ref('user/'+ req.session.uid).once('value',
    function(snapshot){
        res.render('user', { title: '會員專區', 
        nickname: snapshot.val().nickname});
    })
})
module.exports = router; 